#######
Hotkeys
#######

VLC Hotkeys are keyboard shortcuts you can use even if VLC does not have the focus. Here, we show you the most used hotkeys and also a full list of the global hotkeys allowed in VLC.

*********
Most Used 
*********
Find below a list of most used global hotkeys in VLC: 

===============  ===============
   Action           Hotkey
---------------  ---------------
Fullscreen       f 
Exit fullscreen  Esc  
Play/Pause       Space
Stop             s
Faster           \+
Slower           \-
Normal rate      =
Next             n
Previous         p 
Volume up        ctrl + Up
Volume down      ctrl + Down 
Mute             m
Jump backward    Shift + Left
Jump forward     Shift + Right
===============  ===============

*********
Full List
*********

.. figure::  /static/images/userguides/hotkey_one.PNG
   :align:   center

.. figure::  /static/images/userguides/hotkey_two.PNG
   :align:   center

.. figure::  /static/images/userguides/hotkey_three.PNG
   :align:   center

.. figure::  /static/images/userguides/hotkey_four.PNG
   :align:   center

.. figure::  /static/images/userguides/hotkey_five.PNG
   :align:   center

.. figure::  /static/images/userguides/hotkey_six.PNG
   :align:   center
